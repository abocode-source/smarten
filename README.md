# smarten
高性能的分布式配置管理中心

- 配置管理中心提供系统参数配置管理，例如业务数据开关、数据库的配置信息等，配置参数修改以后可以实时推送到客户端(业务系统或者服务)，方便系统动态修改运行参数。
- 支持多个业务系统，每个系统可以配置多种环境如：（development、test、production）。
- 所有参数由development environment配置，test和production 继承development environment配置，也可以覆盖其配置，test和production environment只提供修改功能。
- 业务系统会缓存配置信息到本地文件系统，如果server不可用，可以使用本地备份。client 能够定时重连server，保证配置中心可用。
- 配置中心备份配置到文件系统，如果数据库不用，能够保证对客户端提供配置信息。

HOW 怎么使用：
----------------------
* 启动服务

在项目中smarten-server-web运行SmartenServer.java


HOW 项目中集成：
----------------------
* POM配置

```xml
  <dependency>
        <groupId>com.abocode.smarten</groupId>
        <artifactId>smarten-client</artifactId>
        <version>${project.parent.version}</version>
   </dependency>
```

<b>方式一：spring mvc Controller</b>
```
    @Slf4j
    @RestController
    @RequestMapping("/tests")
    public class HelloController {
        @Autowired
       private Properties properties;
        @Value(" ${string}")
        private String  val;
        @GetMapping
        public String hello() {
            return properties.getProperty("string");
        }
    }
    
```
<b>方式二</b>
```
    String host="127.0.0.1";
    int port=5555;
    String projectCode="smarten";
    String moduleCodes="smarten-test"; //多个用逗号隔开
    String profile="development";
    ClientProperties clientProperties = new ClientProperties(host,port,projectCode,moduleCodes,profile);
    clientProperties.addConfigurationListener(new ConfigTestListener());
    for (int i=0;i<100000;i++){
        Thread.sleep(2000);
        System.out.println(clientProperties.getString("string"));
    }
```
<b>方式三：spring  boot</b>
``` 
    @Bean
    public Properties properties() {
         String host="127.0.0.1";
         int port=5555;
         String projectCode="smarten";
         String moduleCodes="smarten-test"; //多个用逗号隔开
         String profile="development";
        return new ClientProperties(host,port,projectCode,moduleCodes,profile).getProperties();
    }
    @Bean
    public  PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        PropertySourcesPlaceholderConfigurer bean=new PropertySourcesPlaceholderConfigurer();
        bean.setProperties(properties());
       return bean;
    }    
```

Rest 接口获取配置：
----------------------
配置信息：

http://127.0.0.1:7077/swagger-ui.html

测试链接：

http://127.0.0.1:7078/swagger-ui.html

