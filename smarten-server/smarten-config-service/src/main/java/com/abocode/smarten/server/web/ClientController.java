/**        
 * Copyright (c) 2017 by abocode.com.
 */    
package com.abocode.smarten.server.web;

import com.abocode.smarten.rpc.server.netty.ServerHandler;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Slf4j
@RestController
@RequestMapping("/clients")
@Api(value = "组件信息", tags = "组件信息")
public class ClientController {
	@GetMapping()
	public Object queryClients() {
		return  ServerHandler.getClients().entrySet();
	}
}
