package com.abocode.smarten.server.api.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author: guanxianfei
 * @date: 2019/1/5
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateConfigCommand {
    @NotNull
    @ApiModelProperty(value = "配置参数类型",notes = "配置参数类型",required = true)
    private  String type;
    @ApiModelProperty(value = "配置参数Code",notes = "配置参数Code",required = true)
    private  String code;
    @ApiModelProperty(value = "配置参数值",notes = "配置参数值",required = true)
    private  String value;
    @NotNull
    @Size(min = 3,max = 20)
    @ApiModelProperty(value = "配置参数名称",notes = "配置参数名称",required = true)
    private  String name;
    @ApiModelProperty(value = "配置参数单位",notes = "配置参数单位")
    private String unit;
    @ApiModelProperty(value = "配置参数描述",notes = "配置参数描述")
    private  String description;
    @ApiModelProperty(value = "项目编码",notes = "项目编码")
    private  String  projectCode;
    @ApiModelProperty(value = "模块编码",notes = "模块编码")
    private  String  moduleCode;
    @ApiModelProperty(value = "使用环境",notes = "使用环境")
    private  String  profile;
}
