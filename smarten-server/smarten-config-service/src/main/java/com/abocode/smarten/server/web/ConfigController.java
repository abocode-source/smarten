package com.abocode.smarten.server.web;

import com.abocode.smarten.server.api.command.CreateConfigCommand;
import com.abocode.smarten.server.api.command.GetConfigCommand;
import com.abocode.smarten.server.api.command.UpdateConfigCommand;
import com.abocode.smarten.server.service.ConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Description:
 *
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Slf4j
@RestController
@RequestMapping("/configs")
@Api(value = "配置信息", tags = "配置信息")
public class ConfigController {
    @Autowired
    private ConfigService configService;

    @PostMapping(value = "/add")
    @ApiOperation(value = "添加配置信息", notes = "添加配置信息")
    public void create(@RequestBody @Valid CreateConfigCommand command) {
        configService.add(command);
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "修改配置信息", notes = "修改配置信息")
    public void update(@RequestBody @Valid UpdateConfigCommand command) {
        configService.update(command.getId(), command);
    }

    /**
     * @param id
     * @return
     */
    @GetMapping(value = "/findOne")
    @ApiOperation(value = "根据配置信息id获取配置信息", notes = "根据配置信息id获取配置信息")
    public GetConfigCommand get(Long id) {
        return configService.find(id);
    }


    @GetMapping(value = "/findByCode")
    @ApiOperation(value = "根据code获得配置信息", notes = "根据code获得配置信息")
    public String findByCode(String code) {
        return configService.findValueByCode(code);
    }
}
