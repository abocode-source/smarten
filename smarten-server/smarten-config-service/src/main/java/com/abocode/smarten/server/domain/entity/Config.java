package com.abocode.smarten.server.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author: guanxianfei
 * @date: 2019/1/5
 */
@Entity
@Table(name = "config")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Config  extends AbstractColumnCreateAndUpdateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 12)
    private Long id;
    @Column(nullable = false,columnDefinition="varchar(50) COMMENT '参数名称'")
    private  String name;
    @Column(unique = true,nullable = false,columnDefinition="varchar(1000) COMMENT '参数Code'")
    private  String code;
    @Column(columnDefinition="varchar(2000) COMMENT '参数值'")
    private  String value;
    @Column(columnDefinition="varchar(8) COMMENT '单位'")
    private String unit;
    @Column(length = 100,columnDefinition="varchar(100) COMMENT '描述'")
    private  String description;
    @Column(nullable = false,columnDefinition="varchar(50) COMMENT '项目编码'")
    private  String  projectCode;
    @Column(nullable = false,columnDefinition="varchar(50) COMMENT '模块编码'")
    private  String  moduleCode;
    @Column(nullable = false,columnDefinition="varchar(50) COMMENT '使用环境'")
    private  String  profile;
}
