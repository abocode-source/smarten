package com.abocode.smarten.server.admin;

import com.abocode.smarten.server.admin.configuration.ServerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author guanxf
 */
@SpringBootApplication
@Import({ServerConfiguration.class})
@EnableJpaAuditing
@EnableTransactionManagement
public class SmartenAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(SmartenAdminApplication.class, args);
    }
}
