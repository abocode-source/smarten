package com.abocode.smarten.server.admin.configuration;

import com.abocode.smarten.rpc.server.netty.NettyServer;
import com.abocode.smarten.rpc.server.netty.ServerHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Configuration
public class ServerConfiguration {
    @Value(" ${netty.server.port}")
    private int port;
    @Bean
    public ServerHandler serverHandler() {
        return new ServerHandler();
    }
    @Bean
    public NettyServer configServer() {
        return new NettyServer(serverHandler(),port);
    }
}
