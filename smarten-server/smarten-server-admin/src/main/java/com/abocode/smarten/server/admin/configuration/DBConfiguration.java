package com.abocode.smarten.server.admin.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Configuration
public class DBConfiguration {

    @Value("${database.server.url}")
    private String databaseUrl;

    @Value("${database.server.driver}")
    private String databaseDriver;

    @Value("${database.server.username}")
    private String username;

    @Value("${database.server.password}")
    private String password;

    @Value("${database.server.maxActive}")
    private int maxActive;

    @Value("${database.server.maxIdle}")
    private int maxIdle;

    @Value("${database.server.removeAbandonedTimeout}")
    private int removeAbandonedTimeout;

    @Value("${database.server.maxWait}")
    private int maxWait;
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public DruidDataSource dataSource() {
        DruidDataSource  dataSource = new DruidDataSource();
        dataSource.setUrl(databaseUrl);
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setUrl(databaseUrl);
        dataSource.setMaxActive(maxActive);
        dataSource.setMinIdle(maxIdle);
        dataSource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
        return  dataSource;
    }
    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }
}
