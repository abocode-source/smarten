package com.abocode.smarten.server.admin.api.util;

import com.abocode.smarten.server.admin.api.vo.PageQueryVO;
import com.abocode.smarten.server.api.PageQueryResponse;
import org.springframework.data.domain.Page;

/**
 * @author guanxf
 */
public abstract class PageQueryUtils {
    public static void pageResultMapper(Page page, PageQueryVO dto, PageQueryResponse pageVo) {
        int currentPage = dto.getPage();
        int totalPage = page.getTotalPages();
        pageVo.setPage(currentPage);
        pageVo.setCurrentPageElements(page.getNumberOfElements());
        pageVo.setSize(dto.getSize());
        pageVo.setTotalPage(totalPage);
        pageVo.setTotalElements(page.getTotalElements());
        pageVo.setPrevPage(currentPage > 1 ? currentPage - 1 : 1);
        pageVo.setFirstPage(currentPage == 1);
        pageVo.setNextPage(currentPage < totalPage ? currentPage + 1 : totalPage > 0 ? totalPage : 1);
        pageVo.setLastPage(currentPage == totalPage || totalPage == 0);
    }
}
