package com.abocode.smarten.server.admin.domain.repository;

import com.abocode.smarten.server.admin.domain.entity.User;

/**
 * @author guanxf
 */
public interface UserRepository extends BaseRepository<User> {
    User findByUsername(String username);
}
