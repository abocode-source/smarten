package com.abocode.smarten.server.admin.license;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;

/**
 * @author guanxf
 */
public abstract class LicenseUtils {
    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOBX4XfYKzl6j2pCZZQUDNObja8FG6X7LvHksLxmGQACTji4qAlty4irgaZDW3RHATtgiszkeoBhHNQ4YZSwCjuSiZjrZ+ZPEGvbRMY6zBJ7KZbZZmhLCax7cD2/HOpu/95zuTiXvFZVyUdkzbGALg5M8k8QboRdDBXydDYCDc+wIDAQAB";
    private static final String LICENSE = "license.rbl";
    public static AuthorityConfig authorityConfig;
    private static String status;

    /**
     * 获取本机的序列号
     */
    public static String serial() {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("cat /sys/class/dmi/id/product_uuid");
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String uuid = br.readLine();
            br.close();
            isr.close();
            is.close();
            if (StringUtils.isNotBlank(uuid)) {
                return DigestUtils.md5Hex(uuid);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 过期时间
     */
    public static String expiredDate() {
        if (authorityConfig != null) {
            return DateFormatUtils.format(authorityConfig.getExpiredDate(), "yyyy-MM-dd HH:mm:ss");
        }
        return null;
    }

    /**
     * 生效时间
     */
    public static String startDate() {
        if (authorityConfig != null) {
            return DateFormatUtils.format(authorityConfig.getStartDate(), "yyyy-MM-dd HH:mm:ss");
        }
        return null;
    }

    /**
     * 是否过期
     */
    public static boolean isExpired() {
        if (authorityConfig != null) {
            long now = System.currentTimeMillis();
            if (authorityConfig.getStartDate().getTime() > now) {
                status = "license ineffective";
                return true;
            }
            if (authorityConfig.getExpiredDate().getTime() < now) {
                status = "license expired";
                return true;
            }
            status = "license check success";
            return false;
        }
        return true;
    }


    /**
     * 是否已经激活
     */
    public static boolean isActive() {
        return authorityConfig != null;
    }

    /**
     * 当前状态
     */
    public static String getStatus() {
        return status;
    }

    /**
     * 获取配置
     */
    public static AuthorityConfig getAuthorityConfig() {
        return authorityConfig;
    }

    public static boolean loadLicense() {
        return loadLicense(serial());
    }

    public static boolean loadLicense(String serial) {
        authorityConfig = readLicense(serial);
        return authorityConfig != null;
    }

    private static AuthorityConfig readLicense(String serial) {
        try (FileInputStream fileInputStream = new FileInputStream(new File(LICENSE))) {
            byte[] encryptedKey = new byte[128];
            byte[] sign = new byte[128];
            fileInputStream.read(encryptedKey);
            fileInputStream.read(sign);
            byte[] encryptedContent = new byte[fileInputStream.available()];
            fileInputStream.read(encryptedContent);
            String key = decryptRSA(encryptedKey);
            String content = decryptAES(encryptedContent, key);
            if (verifyRSA(content, sign)) {
                AuthorityConfig authorityConfig = JSONObject.parseObject(content, AuthorityConfig.class);
                if (serial != null && serial.equalsIgnoreCase(authorityConfig.getSerial())) {
                    status = "license check success";
                    return authorityConfig;
                } else {
                    status = "serial not match";
                }
            } else {
                status = "RSA verify failed";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            status = "license not found";
        } catch (IOException e) {
            e.printStackTrace();
            status = "license read failed";
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | InvalidKeySpecException | IllegalBlockSizeException e) {
            e.printStackTrace();
            status = "RSA or AES decrypt failed";
        } catch (SignatureException e) {
            e.printStackTrace();
            status = "RSA verify failed";
        }
        return null;
    }

    private static String decryptRSA(byte[] encrypted) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(PUBLIC_KEY));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(2, keyFactory.generatePublic(spec));
        return new String(cipher.doFinal(encrypted));
    }

    private static String decryptAES(byte[] str, String key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, NoSuchAlgorithmException {
        SecretKey deskey = new SecretKeySpec(key.getBytes(), "AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, deskey);
        return new String(c.doFinal(str));
    }

    private static boolean verifyRSA(String content, byte[] signature) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(PUBLIC_KEY));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(keyFactory.generatePublic(spec));
        publicSignature.update(content.getBytes(StandardCharsets.UTF_8));
        return publicSignature.verify(signature);
    }

    public static class AuthorityConfig {
        private String serial;
        private Date expiredDate;
        private Date startDate;
        private int channel;
        private int order;
        private int peer;

        public Date getStartDate() {
            return startDate;
        }

        public AuthorityConfig setStartDate(Date startDate) {
            this.startDate = startDate;
            return this;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public Date getExpiredDate() {
            return expiredDate;
        }

        public void setExpiredDate(Date expiredDate) {
            this.expiredDate = expiredDate;
        }

        public int getChannel() {
            return channel;
        }

        public void setChannel(int channel) {
            this.channel = channel;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public int getPeer() {
            return peer;
        }

        public void setPeer(int peer) {
            this.peer = peer;
        }
    }
}
