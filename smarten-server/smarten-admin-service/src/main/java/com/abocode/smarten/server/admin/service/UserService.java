package com.abocode.smarten.server.admin.service;

import com.abocode.smarten.server.admin.api.command.CreateUserCommand;
import com.abocode.smarten.server.admin.api.vo.UserPageQueryVO;
import com.abocode.smarten.server.api.CommonResponse;
import com.abocode.smarten.server.api.PageQueryResponse;

/**
 * @author guanxf
 */
public interface UserService {
    CommonResponse createUser(CreateUserCommand createUserCommand);

    CommonResponse alterUser(CreateUserCommand createUserCommand);

    CommonResponse userDetail(Long id);

    PageQueryResponse userList(UserPageQueryVO userPageQueryDto);
}
