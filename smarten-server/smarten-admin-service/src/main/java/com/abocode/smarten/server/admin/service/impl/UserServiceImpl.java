package com.abocode.smarten.server.admin.service.impl;

import com.abocode.smarten.server.admin.api.command.CreateUserCommand;
import com.abocode.smarten.server.admin.api.vo.UserPageQueryVO;
import com.abocode.smarten.server.admin.domain.entity.User;
import com.abocode.smarten.server.admin.domain.repository.UserRepository;
import com.abocode.smarten.server.admin.api.util.PageQueryUtils;
import com.abocode.smarten.server.admin.service.UserService;
import com.abocode.smarten.server.api.CommonResponse;
import com.abocode.smarten.server.api.PageQueryResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

/**
 * @author guanxf
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserRepository userRepository;
    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public CommonResponse createUser(CreateUserCommand createUserCommand) {
        CommonResponse commonResponse = new CommonResponse();
        String username = createUserCommand.getUsername();
        if (userRepository.findByUsername(username) != null) {
            return commonResponse.setStatus(CommonResponse.FAIL_STATUS).setMessage("帐号已存在");
        }
        User user = new User();
        return getCommonResponse(createUserCommand, commonResponse, user);
    }

    @Override
    public CommonResponse alterUser(CreateUserCommand createUserCommand) {
        CommonResponse commonResponse = new CommonResponse();
        Optional<User> optional = userRepository.findById(createUserCommand.getId());
        if (!optional.isPresent()) {
            return commonResponse.setStatus(CommonResponse.FAIL_STATUS).setMessage("帐号不存在");
        }
        User user = optional.get();
        return getCommonResponse(createUserCommand, commonResponse, user);
    }

    @Override
    public CommonResponse userDetail(Long id) {
        Optional<User> optional = userRepository.findById(id);
        CommonResponse commonResponse = new CommonResponse();
        if (!optional.isPresent()) {
            return commonResponse.setStatus(CommonResponse.FAIL_STATUS).setMessage("帐号不存在");
        }
        return commonResponse.setData(optional.get());
    }

    @Override
    public PageQueryResponse userList(UserPageQueryVO userPageQueryDto) {
        Pageable page = PageRequest.of(userPageQueryDto.getPage() - 1, userPageQueryDto.getSize(), Sort.Direction.fromString("DESC"), "id");
        Page<User> user = userRepository.findAll((root, query, cb) -> {
            Predicate predicate = root.isNotNull();
            String search = userPageQueryDto.getSearch();
            if (StringUtils.isNotBlank(search)) {
                Predicate username = cb.like(root.get("username").as(String.class), "%" + search + "%");
                Predicate name = cb.like(root.get("name").as(String.class), "%" + search + "%");
                Predicate or = cb.or(username, name);
                predicate = cb.and(predicate, or);
            }
            String enable = userPageQueryDto.getEnable();
            if (StringUtils.isNotBlank(enable)) {
                Predicate equal = cb.equal(root.get("enable").as(boolean.class), Boolean.valueOf(enable));
                predicate = cb.and(predicate, equal);
            }
            return predicate;
        }, page);
        PageQueryResponse<User> userPageQueryResponse = new PageQueryResponse<>();
        PageQueryUtils.pageResultMapper(user, userPageQueryDto, userPageQueryResponse);
        userPageQueryResponse.setContent(user.getContent());
        return userPageQueryResponse;
    }

    private CommonResponse getCommonResponse(CreateUserCommand createUserCommand, CommonResponse commonResponse, User user) {
        Date start, end;
        try {
            end = DateUtils.parseDate(createUserCommand.getEndDate(), Locale.CHINA, "yyyy-MM-dd");
            start = DateUtils.parseDate(createUserCommand.getStartDate(), Locale.CHINA, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
            return commonResponse.setStatus(CommonResponse.FAIL_STATUS).setMessage("无效的日期");
        }
        BeanUtils.copyProperties(createUserCommand, user);
        user.setStartDate(start);
        user.setEndDate(end);
        String password = user.getPassword();
        if (StringUtils.isNotBlank(password)) {
            user.setPassword(passwordEncoder.encode(password));
        }
        user.setEnable(Boolean.valueOf(createUserCommand.getEnable()));
        userRepository.saveAndFlush(user);
        return commonResponse;
    }
}
