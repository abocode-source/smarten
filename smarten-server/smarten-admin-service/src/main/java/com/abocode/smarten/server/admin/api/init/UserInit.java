package com.abocode.smarten.server.admin.api.init;

import com.abocode.smarten.server.admin.domain.entity.User;
import com.abocode.smarten.server.admin.domain.repository.UserRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Locale;

/**
 * @author guanxf
 */
@Component
public class UserInit {
    @Resource
    private UserRepository userRepository;
    @Resource
    private PasswordEncoder passwordEncoder;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        if (userRepository.findByUsername("admin") != null) {
            return;
        }
        User user = new User();
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("admin"));
        try {
            user.setStartDate(DateUtils.parseDate("2019-01-01", Locale.CHINA, "yyyy-MM-dd"));
            user.setEndDate(DateUtils.parseDate("2020-01-01", Locale.CHINA, "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        user.setEnable(true);
        user.setName("admin");
        userRepository.saveAndFlush(user);
    }
}
