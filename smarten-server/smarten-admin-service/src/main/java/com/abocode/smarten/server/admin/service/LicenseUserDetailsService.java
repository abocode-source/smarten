package com.abocode.smarten.server.admin.service;
import com.abocode.smarten.server.admin.domain.entity.User;
import com.abocode.smarten.server.admin.domain.repository.UserRepository;
import com.abocode.smarten.server.security.UserDetailsImpl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author guanxf
 */
@Component
public class LicenseUserDetailsService implements UserDetailsService {
    @Resource
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username + " not found");
        }
        UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setUsername(user.getUsername());
        userDetails.setPassword(user.getPassword());
        userDetails.setName(user.getName());
        userDetails.setEnable(user.isEnable());
        if (user.getUsername().equalsIgnoreCase("admin")) {
            userDetails.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
        } else {
            userDetails.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
        }
        Long startDate = user.getStartDate().getTime();
        Long endDate = user.getEndDate().getTime();
        Long now = new Date().getTime();
        boolean accountNonExpired = true;
        if (now < startDate || now > endDate) {
            accountNonExpired = false;
        }
        userDetails.setAccountNonExpired(accountNonExpired);
        return userDetails;
    }
}
