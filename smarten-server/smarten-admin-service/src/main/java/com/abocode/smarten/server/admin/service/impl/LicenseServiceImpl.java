package com.abocode.smarten.server.admin.service.impl;

import com.abocode.smarten.server.admin.license.LicenseGenerator;
import com.abocode.smarten.server.api.CommonResponse;
import com.abocode.smarten.server.api.PageQueryResponse;
import com.abocode.smarten.server.security.UserDetailsImpl;
import com.abocode.smarten.server.admin.service.LicenseService;
import com.abocode.smarten.server.admin.api.command.CreateLicenseCommand;
import com.abocode.smarten.server.admin.api.vo.LicensePageQueryVO;
import com.abocode.smarten.server.admin.domain.entity.License;
import com.abocode.smarten.server.admin.domain.repository.LicenseRepository;
import com.abocode.smarten.server.admin.api.util.PageQueryUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.Optional;
import java.util.UUID;

/**
 * @author guanxf
 */
@Service
public class LicenseServiceImpl implements LicenseService {
    @Resource
    private LicenseRepository licenseRepository;

    @Override
    public CommonResponse generate(CreateLicenseCommand createLicenseCommand) {
        CommonResponse commonResponse = new CommonResponse();
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String uuid = UUID.randomUUID().toString();
        String generate = LicenseGenerator.generate(createLicenseCommand, uuid);
        if ("success".equalsIgnoreCase(generate)) {
            License license = new License();
            BeanUtils.copyProperties(createLicenseCommand, license);
            license.setUuid(uuid);
            license.setUser(userDetails.getUsername());
            license.setOrderer(createLicenseCommand.getOrder());
            licenseRepository.saveAndFlush(license);
        } else {
            commonResponse.setStatus(CommonResponse.FAIL_STATUS);
            commonResponse.setMessage(generate);
        }
        return commonResponse;
    }

    @Override
    public PageQueryResponse licenseList(LicensePageQueryVO pageQueryDto) {
        Pageable page = PageRequest.of(pageQueryDto.getPage() - 1, pageQueryDto.getSize(), Sort.Direction.fromString("DESC"), "id");
        Page<License> user = licenseRepository.findAll((root, query, cb) -> {
            Predicate predicate = root.isNotNull();
            String name = pageQueryDto.getName();
            if (StringUtils.isNotBlank(name)) {
                Predicate accountPredicate = cb.like(root.get("user").as(String.class), "%" + name + "%");
                predicate = cb.and(predicate, accountPredicate);
            }
            return predicate;
        }, page);
        PageQueryResponse<License> licensePageQueryResponse = new PageQueryResponse<>();
        PageQueryUtils.pageResultMapper(user, pageQueryDto, licensePageQueryResponse);
        licensePageQueryResponse.setContent(user.getContent());
        return licensePageQueryResponse;
    }

    @Override
    public CommonResponse licenseDetail(Long id) {
        CommonResponse commonResponse = new CommonResponse();
        if (id != null) {
            Optional<License> optional = licenseRepository.findById(id);
            if (optional.isPresent()) {
                License license = optional.get();
                return commonResponse.setData(license);
            }
        }
        commonResponse.setStatus(CommonResponse.FAIL_STATUS);
        commonResponse.setMessage("查询失败");
        return commonResponse;
    }
}
