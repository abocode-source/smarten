package com.abocode.smarten.server.admin.api.command;

import javax.validation.constraints.NotNull;

/**
 * @author guanxf
 */
public class CreateLicenseCommand {
    @NotNull
    private String serial;
    @NotNull
    private String startDate;
    @NotNull
    private String endDate;
    @NotNull
    private int channel;
    @NotNull
    private int order;
    @NotNull
    private int peer;
    private String contract;
    @NotNull
    private String version;
    private String remark;
    @NotNull
    private String company;

    public String getCompany() {
        return company;
    }

    public CreateLicenseCommand setCompany(String company) {
        this.company = company;
        return this;
    }

    public String getSerial() {
        return serial;
    }

    public CreateLicenseCommand setSerial(String serial) {
        this.serial = serial;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public CreateLicenseCommand setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public CreateLicenseCommand setEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public int getChannel() {
        return channel;
    }

    public CreateLicenseCommand setChannel(int channel) {
        this.channel = channel;
        return this;
    }

    public int getOrder() {
        return order;
    }

    public CreateLicenseCommand setOrder(int order) {
        this.order = order;
        return this;
    }

    public int getPeer() {
        return peer;
    }

    public CreateLicenseCommand setPeer(int peer) {
        this.peer = peer;
        return this;
    }

    public String getContract() {
        return contract;
    }

    public CreateLicenseCommand setContract(String contract) {
        this.contract = contract;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public CreateLicenseCommand setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public CreateLicenseCommand setRemark(String remark) {
        this.remark = remark;
        return this;
    }
}
