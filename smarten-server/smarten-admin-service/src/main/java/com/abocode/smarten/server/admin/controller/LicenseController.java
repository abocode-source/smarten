package com.abocode.smarten.server.admin.controller;

import com.abocode.smarten.server.admin.api.command.CreateLicenseCommand;
import com.abocode.smarten.server.admin.api.vo.LicensePageQueryVO;
import com.abocode.smarten.server.admin.service.LicenseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;

/**
 * @author guanxf
 */
@RestController("/licenses")
public class LicenseController {
    @Resource
    private LicenseService licenseService;

//    @ApiOperation(value = "生成license", notes = "AES密钥为16位，错误或不填使用默认密钥")
    @PostMapping
    public ResponseEntity generate(@RequestBody @Valid CreateLicenseCommand createLicenseCommand) {
        return ResponseEntity.ok(licenseService.generate(createLicenseCommand));
    }

//    @ApiOperation(value = "下载license")
    @GetMapping("/downloadLicense")
    public void downloadLicense(HttpServletResponse response, String uuid) {
        download(response, "data/" + uuid + "/license.rbl");
    }

//    @ApiOperation(value = "")
    @GetMapping("/downloadSerialViewer")
    public void downloadSerialViewer(HttpServletResponse response) {
        download(response, "data/serial-viewer.jar");
    }

//    @ApiOperation(value = "license记录分页查询")
    @GetMapping("/licenseList")
    public ResponseEntity licenseList(LicensePageQueryVO pageQueryDto) {
        return ResponseEntity.ok(licenseService.licenseList(pageQueryDto));
    }

//    @ApiOperation(value = "license记录详情")
    @GetMapping("/licenseDetail")
    public ResponseEntity licenseDetail(Long id) {
        return ResponseEntity.ok(licenseService.licenseDetail(id));
    }

    private void download(HttpServletResponse response, String file) {
        try (InputStream inputStream = new FileInputStream(file);
             BufferedInputStream bis = new BufferedInputStream(inputStream)) {
            response.setContentType("application/octet-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + file.substring(file.lastIndexOf("/") + 1));// 设置文件名
            byte[] buffer = new byte[1024];
            OutputStream os = response.getOutputStream();
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer, 0, i);
                i = bis.read(buffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
