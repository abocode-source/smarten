package com.abocode.smarten.server.admin.api.vo;

/**
 * @author guanxf
 */
public class UserPageQueryVO extends PageQueryVO {
    private String enable;
    private String search;

    public String getEnable() {
        return enable;
    }

    public UserPageQueryVO setEnable(String enable) {
        this.enable = enable;
        return this;
    }

    public String getSearch() {
        return search;
    }

    public UserPageQueryVO setSearch(String search) {
        this.search = search;
        return this;
    }
}
