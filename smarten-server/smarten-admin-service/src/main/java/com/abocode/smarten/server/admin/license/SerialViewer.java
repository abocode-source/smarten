package com.abocode.smarten.server.admin.license;

/**
 * @author guanxf
 * 用于生成license-viewer.jar
 */
public class SerialViewer {
    public static void main(String[] args) {
        System.out.println("serial:" + LicenseUtils.serial());
    }
}
