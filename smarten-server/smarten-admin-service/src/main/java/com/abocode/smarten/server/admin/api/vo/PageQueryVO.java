package com.abocode.smarten.server.admin.api.vo;


/**
 * @author guanxf
 */
public class PageQueryVO {
    /**
     * 页码
     */
    private int page = 1;

    /**
     * 分页大小
     */
    private int size = 10;


    public PageQueryVO() {

    }

    public PageQueryVO(int page, int size) {
        this.page = page;
        this.size = size;
    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
