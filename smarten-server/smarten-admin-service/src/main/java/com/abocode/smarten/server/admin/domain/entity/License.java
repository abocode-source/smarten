package com.abocode.smarten.server.admin.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author guanxf
 */
@Entity
@Table(name = "t_license")
public class License extends BaseEntity {
    private String serial;
    private String startDate;
    private String endDate;
    private int channel;
    private int orderer;
    private int peer;
    private String contract;
    private String version;
    private String remark;
    private String company;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time = new Date();
    private String user;
    private String uuid;

    public String getUuid() {
        return uuid;
    }

    public License setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getSerial() {
        return serial;
    }

    public License setSerial(String serial) {
        this.serial = serial;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public License setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public License setEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public int getChannel() {
        return channel;
    }

    public License setChannel(int channel) {
        this.channel = channel;
        return this;
    }

    public int getOrderer() {
        return orderer;
    }

    public License setOrderer(int orderer) {
        this.orderer = orderer;
        return this;
    }

    public int getPeer() {
        return peer;
    }

    public License setPeer(int peer) {
        this.peer = peer;
        return this;
    }

    public String getContract() {
        return contract;
    }

    public License setContract(String contract) {
        this.contract = contract;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public License setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public License setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public License setCompany(String company) {
        this.company = company;
        return this;
    }

    public Date getTime() {
        return time;
    }

    public License setTime(Date time) {
        this.time = time;
        return this;
    }

    public String getUser() {
        return user;
    }

    public License setUser(String user) {
        this.user = user;
        return this;
    }
}
