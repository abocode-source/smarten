package com.abocode.smarten.server.admin.api.vo;

/**
 * @author guanxf
 */
public class LicensePageQueryVO extends PageQueryVO {
    private String name;

    public String getName() {
        return name;
    }

    public LicensePageQueryVO setName(String name) {
        this.name = name;
        return this;
    }
}
