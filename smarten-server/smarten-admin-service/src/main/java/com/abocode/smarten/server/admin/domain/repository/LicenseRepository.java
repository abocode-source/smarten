package com.abocode.smarten.server.admin.domain.repository;

import com.abocode.smarten.server.admin.domain.entity.License;

/**
 * @author guanxf
 */
public interface LicenseRepository extends BaseRepository<License> {
}
