package com.abocode.smarten.server.admin.license;

import com.alibaba.fastjson.JSON;
import com.abocode.smarten.server.admin.api.command.CreateLicenseCommand;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.ParseException;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;

/**
 * @author guanxf
 */
public abstract class LicenseGenerator {
    private static final String PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAI4Ffhd9grOXqPakJllBQM05uNrwUbpfsu8eSwvGYZAAJOOLioCW3LiKuBpkNbdEcBO2CKzOR6gGEc1DhhlLAKO5KJmOtn5k8Qa9tExjrMEnspltlmaEsJrHtwPb8c6m7/3nO5OJe8VlXJR2TNsYAuDkzyTxBuhF0MFfJ0NgINz7AgMBAAECgYBD70Kqj+IWcERlfo3NghYyVRskt3IQubhJA2+YeVOv4zHCGrys1/1K1ShNj4PWX4lRFHE/4AiMyiJ30oXx0VvDN1A/J4QDj9Brl31Y8E0rtIRzJcAGD15dbCNRusTX4zsIo9w9ci/0/hsX2KnuJeP7TZYN3pCyox66y8Hw3G9mWQJBAP4XuhFegMy5qoE1XHqfV8pdOW8IlZHcg12dT36XwgLb05yl3aow/Vjsb4egQlp1dMLuEdH5MwApnTbJrsiHm/8CQQCPFmfj8yNloqjxBmS2oCpASQnnxvj1mfulP/KW//zpkKR/k2ez2mn9l2IGbmnBBaBRQ6s39KLsnVWbl39sKS8FAkEAvFRbvkn8T0GCJhil/eEi0cP9MNX8/kqiN33EkQDsdO/4TZAG1wFOll1QYQcghymtrFmGbco1yfYOJ7Ce0tTogwJAARvvWjj8QUFthdDC3xeGvFDUntNQArda2AES5FC/qR/R5ptB5ob+41RVEi+w6iodAARCQ4DF8oeVrksSVchL8QJBAJeG/du7qaBD21UXEENFJfVLYBS/fHZ/8SH+Bfq5LPyAN6+l7icbFCzEfkUHUdEJ4oTN0GsD6aFKwPrTJIXURfw=";
    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOBX4XfYKzl6j2pCZZQUDNObja8FG6X7LvHksLxmGQACTji4qAlty4irgaZDW3RHATtgiszkeoBhHNQ4YZSwCjuSiZjrZ+ZPEGvbRMY6zBJ7KZbZZmhLCax7cD2/HOpu/95zuTiXvFZVyUdkzbGALg5M8k8QboRdDBXydDYCDc+wIDAQAB";
    private static final String LICENSE = "license.rbl";
    private static final String AES_KEY = "0123456789012345";

    public static String generate(CreateLicenseCommand createLicenseCommand, String uuid) {
        String msg;
        if (createLicenseCommand.getSerial().length() != 32) {
            msg = "无效的序列号";
            return msg;
        }
        Date start, end;
        try {
            start = DateUtils.parseDate(createLicenseCommand.getStartDate(), Locale.CHINA, "yyyy-MM-dd");
            end = DateUtils.parseDate(createLicenseCommand.getEndDate(), Locale.CHINA, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
            msg = "无效的日期";
            return msg;
        }
        AuthorityConfig authorityConfig = new AuthorityConfig();
        BeanUtils.copyProperties(createLicenseCommand, authorityConfig);
        authorityConfig.setExpiredDate(end);
        authorityConfig.setStartDate(start);
        try {
            generator(authorityConfig, uuid);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | BadPaddingException | SignatureException | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
            msg = "fail";
            return msg;
        }
        msg = "success";
        return msg;
    }

    private static void generator(AuthorityConfig authorityConfig, String uuid) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, IOException {
        String content = JSON.toJSONString(authorityConfig);
        byte[] encryptedKey = encryptRSA();
        byte[] sign = sign(content);
        byte[] encryptedContent = encryptAES(content);
        File file = new File("data/" + uuid + "/" + LICENSE);
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        fileOutputStream.write(encryptedKey);
        fileOutputStream.write(sign);
        fileOutputStream.write(encryptedContent);
        fileOutputStream.close();
    }

    private static byte[] encryptRSA() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(PRIVATE_KEY));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(1, keyFactory.generatePrivate(spec));
        return cipher.doFinal(LicenseGenerator.AES_KEY.getBytes());
    }

    private static byte[] sign(String content) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(PRIVATE_KEY));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(keyFactory.generatePrivate(spec));
        privateSignature.update(content.getBytes(StandardCharsets.UTF_8));
        return privateSignature.sign();
    }

    private static byte[] encryptAES(String str) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, NoSuchAlgorithmException {
        SecretKey deskey = new SecretKeySpec(LicenseGenerator.AES_KEY.getBytes(), "AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, deskey);
        byte[] src = str.getBytes();
        return c.doFinal(src);
    }

    private static class AuthorityConfig {
        private String serial;
        private Date expiredDate;
        private Date startDate;
        private int channel;
        private int order;
        private int peer;

        public Date getStartDate() {
            return startDate;
        }

        public AuthorityConfig setStartDate(Date startDate) {
            this.startDate = startDate;
            return this;
        }

        public String getSerial() {
            return serial;
        }

        public AuthorityConfig setSerial(String serial) {
            this.serial = serial;
            return this;
        }

        public Date getExpiredDate() {
            return expiredDate;
        }

        public AuthorityConfig setExpiredDate(Date expiredDate) {
            this.expiredDate = expiredDate;
            return this;
        }

        public int getChannel() {
            return channel;
        }

        public AuthorityConfig setChannel(int channel) {
            this.channel = channel;
            return this;
        }

        public int getOrder() {
            return order;
        }

        public AuthorityConfig setOrder(int order) {
            this.order = order;
            return this;
        }

        public int getPeer() {
            return peer;
        }

        public AuthorityConfig setPeer(int peer) {
            this.peer = peer;
            return this;
        }
    }
}
