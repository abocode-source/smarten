package com.abocode.smarten.server.admin.service;

import com.abocode.smarten.server.admin.api.command.CreateLicenseCommand;
import com.abocode.smarten.server.admin.api.vo.LicensePageQueryVO;
import com.abocode.smarten.server.api.CommonResponse;
import com.abocode.smarten.server.api.PageQueryResponse;


/**
 * @author guanxf
 */
public interface LicenseService {
    CommonResponse generate(CreateLicenseCommand createLicenseCommand);

    PageQueryResponse licenseList(LicensePageQueryVO pageQueryDto);

    CommonResponse licenseDetail(Long id);
}
