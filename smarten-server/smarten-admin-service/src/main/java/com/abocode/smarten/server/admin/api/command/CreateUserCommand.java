package com.abocode.smarten.server.admin.api.command;

import javax.validation.constraints.NotNull;

/**
 * @author guanxf
 */
public class CreateUserCommand {
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String name;
    private String phone;
    private String email;
    @NotNull
    private String startDate;
    @NotNull
    private String endDate;
    @NotNull
    private String enable;
    private Long id;

    public Long getId() {
        return id;
    }

    public CreateUserCommand setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public CreateUserCommand setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CreateUserCommand setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public CreateUserCommand setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public CreateUserCommand setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CreateUserCommand setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public CreateUserCommand setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public CreateUserCommand setEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public String getEnable() {
        return enable;
    }

    public CreateUserCommand setEnable(String enable) {
        this.enable = enable;
        return this;
    }
}
