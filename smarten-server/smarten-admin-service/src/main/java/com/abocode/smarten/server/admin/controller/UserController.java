package com.abocode.smarten.server.admin.controller;

import com.abocode.smarten.server.admin.api.command.CreateUserCommand;
import com.abocode.smarten.server.admin.api.vo.UserPageQueryVO;
import com.abocode.smarten.server.admin.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author guanxf
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody @Valid CreateUserCommand createUserCommand) {
        return ResponseEntity.ok(userService.createUser(createUserCommand));
    }

    @PostMapping("/alter")
    public ResponseEntity alter(@RequestBody @Valid CreateUserCommand createUserCommand) {
        return ResponseEntity.ok(userService.alterUser(createUserCommand));
    }

    @GetMapping("/list")
    public ResponseEntity list(UserPageQueryVO userPageQueryDto) {
        return ResponseEntity.ok(userService.userList(userPageQueryDto));
    }

    @GetMapping("/detail")
    public ResponseEntity detail(Long id) {
        return ResponseEntity.ok(userService.userDetail(id));
    }
}
