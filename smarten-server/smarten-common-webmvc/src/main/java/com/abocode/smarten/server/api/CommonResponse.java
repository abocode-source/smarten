package com.abocode.smarten.server.api;
import java.io.Serializable;

/**
 * @author guanxf
 */
//@ApiModel(value = "返回对象")
public class CommonResponse implements Serializable {

    public static final int SUCCESS_STATUS = 0;
    public static final String SUCCESS_MESSAGE = "success";

    public static final int FAIL_STATUS = 1;
    public static final String FAIL_MESSAGE = "fail";


//    @ApiModelProperty(value = "消息状态（0：成功，其他表示操作错误）")
    private int status = SUCCESS_STATUS;
//    @ApiModelProperty(value = "消息信息")
    private String message = SUCCESS_MESSAGE;
//    @ApiModelProperty(value = "返回数据")
    private Object data;


    public int getStatus() {
        return status;
    }

    public CommonResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public CommonResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public CommonResponse setData(Object data) {
        this.data = data;
        return this;
    }
}
