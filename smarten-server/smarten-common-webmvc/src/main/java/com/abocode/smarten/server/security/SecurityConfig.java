package com.abocode.smarten.server.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.abocode.smarten.server.admin.api.vo.CommonResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.io.PrintWriter;

/**
 * @author guanxf
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Resource
    private AuthenticationAccessDeniedHandler deniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/user/**").hasAnyRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().loginProcessingUrl("/login")
                .usernameParameter("username").passwordParameter("password")
                .failureHandler((req, resp, e) -> {
                    resp.setContentType("application/json;charset=utf-8");
                    CommonResponse commonResponse = new CommonResponse();
                    commonResponse.setStatus(CommonResponse.FAIL_STATUS);
                    if (e instanceof BadCredentialsException ||
                            e instanceof UsernameNotFoundException) {
                        commonResponse.setMessage("账户名或者密码输入错误!");
                    } else if (e instanceof LockedException) {
                        commonResponse.setMessage("账户被锁定，请联系管理员!");
                    } else if (e instanceof CredentialsExpiredException) {
                        commonResponse.setMessage("密码过期，请联系管理员!");
                    } else if (e instanceof AccountExpiredException) {
                        commonResponse.setMessage("账户过期，请联系管理员!");
                    } else if (e instanceof DisabledException) {
                        commonResponse.setMessage("账户被禁用，请联系管理员!");
                    } else {
                        commonResponse.setMessage("登录失败!");
                    }
                    resp.setStatus(401);
                    PrintWriter out = resp.getWriter();
                    out.write(new ObjectMapper().writeValueAsString(commonResponse));
                    out.flush();
                    out.close();
                })
                .successHandler((req, resp, auth) -> {
                    resp.setContentType("application/json;charset=utf-8");
                    CommonResponse commonResponse = new CommonResponse();
                    commonResponse.setMessage("登录成功!");
                    UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
                    commonResponse.setData(userDetails.getName());
                    PrintWriter out = resp.getWriter();
                    out.write(new ObjectMapper().writeValueAsString(commonResponse));
                    out.flush();
                    out.close();
                })
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler((req, resp, authentication) -> {
                    resp.setContentType("application/json;charset=utf-8");
                    CommonResponse commonResponse = new CommonResponse();
                    commonResponse.setMessage("注销成功!");
                    PrintWriter out = resp.getWriter();
                    out.write(new ObjectMapper().writeValueAsString(commonResponse));
                    out.flush();
                    out.close();
                })
                .permitAll()
                .and()
                .csrf().disable()
                .exceptionHandling().accessDeniedHandler(deniedHandler);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
