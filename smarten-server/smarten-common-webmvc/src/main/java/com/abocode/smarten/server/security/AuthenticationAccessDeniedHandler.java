package com.abocode.smarten.server.security;

import com.abocode.smarten.server.api.CommonResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author guanxf
 */
@Component
public class AuthenticationAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse resp, AccessDeniedException e) throws IOException {
        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.setStatus(CommonResponse.FAIL_STATUS);
        commonResponse.setMessage("权限不足!");
        out.write(new ObjectMapper().writeValueAsString(commonResponse));
        out.flush();
        out.close();
    }
}
