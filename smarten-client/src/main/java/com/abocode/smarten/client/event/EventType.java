package com.abocode.smarten.client.event;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public enum EventType {
    ADD, UPDATE, CLEAR
}
