/**        
 * Copyright (c) 2017 by franky.
 */    
package com.abocode.smarten.client.event;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public interface ConfigurationListener {
	void configurationChanged(ConfigurationEvent event);
}
