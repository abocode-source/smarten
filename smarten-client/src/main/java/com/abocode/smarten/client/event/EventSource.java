/**        
 * Copyright (c) 2017 by franky.
 */
package com.abocode.smarten.client.event;

import com.abocode.smarten.client.netty.NamedThreadFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.*;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public class EventSource {
	private Collection<ConfigurationListener> listeners;
	private static final ExecutorService executorService =new ThreadPoolExecutor(1, 1,
			0L, TimeUnit.MILLISECONDS,
			new LinkedBlockingQueue<Runnable>(),
			new NamedThreadFactory("ConfigEvent"));
	public EventSource() {
		initListeners();
	}

	public void addConfigurationListener(ConfigurationListener configurationListener) {
		checkListener(configurationListener);
		listeners.add(configurationListener);
	}

	public boolean removeConfigurationListener(ConfigurationListener l) {
		return listeners.remove(l);
	}

	public Collection<ConfigurationListener> getConfigurationListeners() {
		return Collections
				.unmodifiableCollection(new ArrayList<ConfigurationListener>(
						listeners));
	}

	public void clearConfigurationListeners() {
		listeners.clear();
	}

	/**
	 * 异步执行ConfigurationListener。
	 * 
	 * @param type
	 * @param propName
	 * @param propValue
	 */
	protected void fireEvent(EventType type, String propName, Object propValue) {
		final Iterator<ConfigurationListener> it = listeners.iterator();
		if (it.hasNext()) {
			final ConfigurationEvent event = createEvent(type, propName, propValue);
			while (it.hasNext()) {
				final ConfigurationListener listener = it.next();
				executorService.submit(new Runnable() {
					@Override
					public void run() {
						listener.configurationChanged(event);
					}
				});
			}
		}
	}

	protected ConfigurationEvent createEvent(EventType type, String propName, Object propValue) {
		return new ConfigurationEvent(this, type, propName, propValue);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		EventSource copy = (EventSource) super.clone();
		copy.initListeners();
		return copy;
	}

	private static void checkListener(Object l) {
		if (l == null) {
			throw new IllegalArgumentException("Listener must not be null!");
		}
	}

	private void initListeners() {
		listeners = new CopyOnWriteArrayList<ConfigurationListener>();
	}
}
