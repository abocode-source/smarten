package com.abocode.smarten.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Properties;

/**
 * 测试代码
 * @author guanxf
 * @date: 2019/7/24
 */
@Slf4j
@RestController
@RequestMapping("/tests")
public class HelloController {
    @Autowired
   private Properties properties;
    @Value(" ${string}")
    private String  val;
    @GetMapping
    public String hello() {
        return properties.getProperty("string");
    }
}
