package com.abocode.smarten.configuration;

import com.abocode.smarten.client.ClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.Properties;

@Configuration
public class ServerConfiguration {

    @Bean
    public Properties properties() {
         String host="127.0.0.1";
         int port=5555;
         String projectCode="smarten";
         String moduleCodes="smarten-test"; //多个用逗号隔开
         String profile="development";
        return new ClientProperties(host,port,projectCode,moduleCodes,profile).getProperties();
    }
    @Bean
    public  PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        PropertySourcesPlaceholderConfigurer bean=new PropertySourcesPlaceholderConfigurer();
        bean.setProperties(properties());
       return bean;
    }



}
