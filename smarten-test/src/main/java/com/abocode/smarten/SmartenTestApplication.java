package com.abocode.smarten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartenTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SmartenTestApplication.class, args);
    }
}

