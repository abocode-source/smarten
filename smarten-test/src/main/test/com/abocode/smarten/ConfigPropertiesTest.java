/**
 * Copyright (c) 2013 by 苏州科大国创信息技术有限公司.
 *
 * Test.java Create on 2013-7-11 下午4:48:04
 */
package com.abocode.smarten;
import com.abocode.smarten.com.abocode.smarten.client.ClientProperties;
import javax.naming.ConfigurationException;

public class ConfigPropertiesTest {

	/**
	 * @param args
	 * @throws ConfigurationException
	 */
	public static void main(String[] args) throws Exception {
		String host="127.0.0.1";
		int port=5555;
		String projectCode="smarten";
		String moduleCodes="smarten-test"; //多个用逗号隔开
		String profile="development";
		ClientProperties clientProperties = new ClientProperties(host,port,projectCode,moduleCodes,profile);
		clientProperties.addConfigurationListener(new ConfigTestListener());
		for (int i=0;i<100000;i++){
			Thread.sleep(2000);
			System.out.println(clientProperties.getString("string"));
		}
		System.in.read();
	}


}
