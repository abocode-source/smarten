package com.abocode.smarten.rpc.server;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
public interface PushService {
   void pushAll(String content);
}
