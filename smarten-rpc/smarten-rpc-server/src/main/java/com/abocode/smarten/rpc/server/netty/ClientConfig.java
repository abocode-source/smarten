package com.abocode.smarten.rpc.server.netty;

import lombok.*;

import java.util.Date;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/23
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientConfig {
    private String address;
    private Date connectTime;
}
