package com.abocode.smarten.client.storage;

import org.springframework.util.Assert;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Description:
 * @author: guanxianfei
 * @date: 2019/7/24
 */
public class FileUtils {
    /**
     * 备份数据到本地
     */
    public static void saveConfigToLocal(String projectCode, String profile, String data) {
        String userHome = System.getProperty("user.home");
        String dataDirectory = userHome + File.separator + ".config" + File.separator + projectCode + File.separator + profile;
        File dir = new File(dataDirectory);
        if(!dir.exists()){
          boolean flag=   dir.mkdirs();
            Assert.isTrue(flag,"创建文件失败");
        }
        File file = new File(dataDirectory + File.separator + "data.properties");
        try {
            writeConfig(file, data.getBytes(StandardCharsets.UTF_8),  false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeConfig(File file,byte[] data, boolean append) throws IOException {
        OutputStream out = null;
        try {
            out = openOutputStream(file, append);
            if (data != null) {
                out.write(data);
            }
            out.close();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IOException(String.format("File %s exists but is a directory",file));
            }
            if (file.canWrite() == false) {
                throw new IOException(String.format("File %s cannot be written",file));
            }
        } else {
            File parent = file.getParentFile();
            if (parent != null) {
                if (!parent.mkdirs() && !parent.isDirectory()) {
                    throw new IOException(String.format(" Directory  %s could not be created",parent));
                }
            }
        }
        return new FileOutputStream(file, append);
    }

    public static String getConfigFromLocal(String projectCode, String profile) throws IOException {
        BufferedReader br = null;
        String userHome = System.getProperty("user.home");
        String fileStr = userHome + File.separator + ".config" + File.separator + projectCode + File.separator + profile + File.separator + "data.properties";
        File file = new File(fileStr);
        if(!file.exists()){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try {

            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String data;
            while((data = br.readLine())!=null) {
                sb.append(data).append("\r\n");
            }
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ioe) {
                // ignore
            }
        }
        return sb.toString();
    }
}
