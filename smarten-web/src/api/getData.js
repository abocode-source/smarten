import axios from 'axios';

export const api = '.';
// 登入
export const login = (data) => axios.post(api + '/login', data).then((response)=>{
    return response
  })
  .catch((error) => {
        return error.response
  });
// 获取serial序列号
export const serial = () => axios.get(api + '/license/downloadSerialViewer');
// 新增license
export const addLicense = (data) => axios.post(api + '/license/generate', data);
// license列表
export const licenseList = (data) => axios.get(api + '/license/licenseList', {
    params: {
        name: data.name,
        page: data.page,
        size: 7
    }
});
// 新增用户
export const addUser = (data) => axios.post(api + '/user/create', data);
// 修改用户
export const reviseUser = (data) => axios.post(api + '/user/alter', data);
// 用户列表
export const userList = (data) => axios.get(api + '/user/list', {
    params: {
        page: data.page,
        size: 7,
        enable: data.enable,
        search: data.search
    }
});

