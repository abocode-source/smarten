import Vue from 'vue';
import Router from 'vue-router';

import Login from '../modules/login/Login';
import Platform from '../modules/platform/Platform';
import userList from '../modules/platform/applyList/userList';
import LiscenseList from '../modules/platform/applyList/LiscenseList';
import AddLiscenseList from '../modules/platform/applyList/AddLiscenseList';

Vue.use(Router);

export default new Router({
    routes: [
        // 重定向
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/platform',
            name: 'platform',
            component: Platform,
            children: [
                {
                    path: '/platform/userList',
                    name: 'userList',
                    component: userList
                },
                // License列表
                {
                    path: '/platform/LiscenseList',
                    name: 'LiscenseList',
                    component: LiscenseList
                },
                {
                    path: '/platform/AddLiscenseList',
                    name: 'AddLiscenseList',
                    component: AddLiscenseList
                },
            ]
        }
    ]
});
