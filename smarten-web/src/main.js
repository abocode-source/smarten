// 作者：小丶张学友
import Vue from 'vue';
import App from './App';
import router from './router';
import moment from 'moment';
// 引入element-ui
import '../theme/index.css';
import ElementUI from 'element-ui';
Vue.use(ElementUI);
// 滚动插件指令之每次加载都回到页面顶部
Vue.directive('scroll', {
    // 当组件完成一次更新时调用 还有其它的钩子函数 自行选择合适的
    componentUpdated: (el) => { el.scrollTop = 0; }
});
// moment
Vue.filter('dateFormat', function (value, formatString) {
    formatString = formatString || 'YYYY-MM-DD HH:mm:ss';
    return moment(value).format(formatString); // value可以是普通日期 20170723
});
// 数字格式化
Vue.filter('initNum', function (num) {
    if (num !== undefined) {
        let a = Math.abs(parseInt(num)).toString().split("");
        let i = a.length - 1;
        while (i >= 3) {
            a.splice(i - 2, 0, ",");
            i = i - 3;
        }
        let b = num < 0 ? ('-' + a.join("")) : a.join("");
        let len = num.toString().length;
        let index = num.toString().indexOf('.');
        return index > -1 ? b + num.toString().slice(index, len) : b;
    }
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
});
